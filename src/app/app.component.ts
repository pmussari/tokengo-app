import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen , NativeStorage } from 'ionic-native';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RootPageService } from './app.rootpage.service';

@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  providers: [RootPageService]
})

export class MyApp {

  rootPage;

  constructor( public platform: Platform, private rootpageService: RootPageService) {

    platform.ready().then(() => {
      NativeStorage.getItem('userToken')
      .then(
        userToken => {
          this.rootPage = userToken ? TabsPage : LoginPage;
          StatusBar.styleDefault();
          Splashscreen.hide();
        },
        error => {
          this.rootPage = LoginPage;
          StatusBar.styleDefault();
          Splashscreen.hide();
        }
      );   
    });

    this.rootpageService.invokeEvent.subscribe((page) => {
      console.log('Go to home');
      if(page == 'home'){
        console.log('Go to home');
        this.rootPage = TabsPage;
      }else{
        this.rootPage = LoginPage;
      }
    });

  }

}

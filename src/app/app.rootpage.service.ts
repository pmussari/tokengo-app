import {Subject} from 'rxjs/Subject';
import { Injectable } from '@angular/core';

@Injectable()
export class RootPageService {

    public invokeEvent:Subject<any> = new Subject();

    constructor() {}
}
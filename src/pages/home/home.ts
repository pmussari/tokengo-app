import { Component, ViewChild, ElementRef } from '@angular/core';

import { NavController } from 'ionic-angular';
import { Geolocation, Geoposition } from 'ionic-native';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  coins: any[];
  
  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad(){
    if(this.map) return; 
    this.loadMap();
  }
 
  loadMap(){

    Geolocation.getCurrentPosition().then((position) => {
 
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 20,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        scrollwheel: false,
        panControl: false,
        rotateControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        scaleControl: false,
        draggable: false,
        disableDoubleClickZoom: true,
        disableDefaultUI:true
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      this.coins = [
        {lat:-34.620042, lng:-58.432972},
        {lat:-34.573691, lng:-58.482131},
        {lat:-34.552934, lng:-58.450929}
      ];

      this.coins.forEach((token) => {
        let coinCoordinates = new google.maps.LatLng(token.lat, token.lng);
        token.marker = new google.maps.Marker({
          map: this.map,
          position: coinCoordinates
        });
      });
      
      let options = {
        frequency: 3000, 
        enableHighAccuracy: true
      };
      Geolocation.watchPosition(options).subscribe((position: Geoposition) => {
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        //this.marker.setPosition(latLng);
        this.map.setCenter(latLng);   
      });

    }, (err) => {
      console.log(err);
    });
  
  }

}

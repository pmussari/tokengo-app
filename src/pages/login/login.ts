import { Component } from '@angular/core';

import { LoadingController } from 'ionic-angular';
import { Facebook , NativeStorage } from 'ionic-native';
import { RootPageService } from '../../app/app.rootpage.service';


@Component({
  templateUrl: 'login.html',
  providers: [RootPageService]
})

export class LoginPage {

  loading;

  constructor(public loadingCtrl: LoadingController, private rootpageService: RootPageService) {

  }

  authorizeUser(token){
  	
  	NativeStorage.setItem('userToken', token)
	  .then(
	    () => {
	    	console.log('authorizeUser');
	    	this.loading.dismiss();
	    	this.rootpageService.invokeEvent.next('home');
	    },
	    error => {
	    	console.error('Error storing item', error);
	    	this.loading.dismiss();
	    }
	  );

  }

  loginFB(){
  	this.loading = this.loadingCtrl.create({
    	content: 'Ingresando...'
  	});
  	this.loading.present();
  	Facebook.getLoginStatus().then((response) => {
  		if(response.status == 'connected'){
  			this.authorizeUser(response.authResponse.accessToken);
  		}else{
		  	Facebook.login(['public_profile','email']).then( (response)=> {
		  		if(response.status == 'connected'){
		  			this.authorizeUser(response.authResponse.accessToken);
		  		}
		  	},( )=>{
		  		this.loading.dismiss();
		  	});		
  		}
  	},( ) => {
  		this.loading.dismiss();
  	});

  }

}

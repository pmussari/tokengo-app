import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { Facebook  , NativeStorage } from 'ionic-native';
import { RootPageService } from '../../app/app.rootpage.service';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
	user: any;

  constructor(public navCtrl: NavController, private rootpageService: RootPageService) {
    Facebook.api('/me?fields=id,name,picture,first_name,last_name,email,birthday',['public_profile','email']).then((response)=>{
      console.log(response);
      this.user = response;
    });
  }

  logout(){
    NativeStorage.remove('userToken')
    .then(
      () => {
        this.rootpageService.invokeEvent.next('login');
      },
      error => {
        console.error('Error removing item', error);
        
      }
    );
  }
}
